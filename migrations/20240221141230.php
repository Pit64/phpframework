<?php

use \Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;

return new class
{
  public function up(Schema $schema): void
  {
    // Ajout des données
    $table = $schema->createTable('users');
    $table->addColumn('id', Types::INTEGER, ['autoincrement' => true, 'unsigned' => true]);
    $table->addColumn('username', Types::STRING, ['length' => 255]);
    $table->addColumn('password', Types::STRING, ['length' => 60]);
    $table->addColumn('created_at', TYPES::DATETIME_IMMUTABLE, ['default' => 'CURRENT_TIMESTAMP']);
    $table->setPrimaryKey(['id']);
  }

  public function down(): void
  {
    // Suppression des données
    echo get_class($this) . ' "down" method called ' . PHP_EOL;
  }
};