<?php

use App\Controller\DashboardController;
use App\Controller\HomeController;
use App\Controller\LoginController;
use App\Controller\PostsController;
use App\Controller\RegistrationController;
use Pit64\Framework\Http\Response;

return [
  ['GET', '/', [HomeController::class, 'index']],
  ['GET', '/posts/{id:\d+}', [PostsController::class, 'show']],
  ['GET', '/posts', [PostsController::class, 'create']],
  ['POST', '/posts', [PostsController::class, 'store']],
  ['GET', '/register', [RegistrationController::class, 'index',
    [
      Pit64\Framework\Http\Middleware\Guest::class
    ]
  ]],
  ['POST', '/register', [RegistrationController::class, 'register']],
  ['GET', '/login', [LoginController::class, 'index',
    [
      Pit64\Framework\Http\Middleware\Guest::class
    ]
  ]],
  ['POST', '/login', [LoginController::class, 'login']],
  ['GET', '/logout', [LoginController::class, 'logout',
    [
      Pit64\Framework\Http\Middleware\Authenticate::class
    ]
  ]],
  ['GET', '/dashboard', [DashboardController::class, 'index',
    [
      Pit64\Framework\Http\Middleware\Authenticate::class,
      Pit64\Framework\Http\Middleware\Dummy::class
    ]
  ]],
  ['GET', '/hello/{name:.+}', function(string $name) {
    return new Response("Hello $name");
  }]
];