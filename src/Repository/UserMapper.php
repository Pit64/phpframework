<?php

namespace App\Repository;

use App\Entity\User;
use Pit64\Framework\Dbal\DataMapper;

class UserMapper
{
  public function __construct(private DataMapper $dataMapper)
  {
  }

  public function save(User $user): void
  {
    $statement = $this->dataMapper->getConnection()->prepare("
      INSERT INTO users
        (username, password, created_at)
      VALUES
        (:username, :password, :created_at)
    ");

    $statement->bindValue(':username', $user->getUsername());
    $statement->bindValue(':password', $user->getPassword());
    $statement->bindValue(':created_at', $user->getCreatedAt()->format('Y-m-d H:i:s'));

    $statement->executeStatement();

    $id = $this->dataMapper->save($user);

    $user->setId($id);
  }
}