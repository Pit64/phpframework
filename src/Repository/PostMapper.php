<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\DBAL\Connection;
use Pit64\Framework\Dbal\DataMapper;

class PostMapper
{
  public function __construct(private DataMapper $dataMapper)
  {
  }

  public function save(Post $post): void
  {
    $statement = $this->dataMapper->getConnection()->prepare("
      INSERT INTO posts
        (title, body, created_at)
      VALUES
        (:title, :body, :created_at)
    ");

    $statement->bindValue(':title', $post->getTitle());
    $statement->bindValue(':body', $post->getBody());
    $statement->bindValue(':created_at', $post->getCreatedAt()->format('Y-m-d H:i:s'));

    $statement->executeStatement();

    $id = $this->dataMapper->save($post);

    $post->setId($id);
  }
}