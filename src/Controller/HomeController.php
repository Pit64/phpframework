<?php

namespace App\Controller;

use App\Widget;
use Pit64\Framework\Controller\AbstractController;
use Pit64\Framework\Http\Response;
use Twig\Environment;

class HomeController extends AbstractController
{
  public function __construct(private Widget $widget)
  {
  }
  public function index(): Response
  {
    return $this->render('home.html.twig');
  }
}