<?php

namespace App\Controller;

use Pit64\Framework\Controller\AbstractController;
use Pit64\Framework\Http\Response;

class DashboardController extends AbstractController
{
  public function index(): Response
  {
    return $this->render('dashboard.html.twig');
  }
}