<?php

namespace App\Controller;

use Pit64\Framework\Authentication\SessionAuthentication;
use Pit64\Framework\Controller\AbstractController;
use Pit64\Framework\Http\RedirectResponse;
use Pit64\Framework\Http\Response;

class LoginController extends AbstractController
{
  public function __construct(private SessionAuthentication $authComponent)
  {
  }

  public function index(): Response
  {
    return $this->render('login.html.twig');
  }

  public function login(): Response
  {
    // Tentative d'identification de l'utilisateur en utilisant un composant de sécurité (booléen)
    // Création de la session de l'utilisateur
    $userIsAuthenticated = $this->authComponent->authenticate(
      $this->request->input('username'),
      $this->request->input('password'),
    );

    // Si cela n'a pas réussi, on prend les infos de l'utilisateur
    if (!$userIsAuthenticated)
    {
      $this->request->getSession()->setFlash('error', 'Mauvaises informations d\'identification');
      return new RedirectResponse('/login');
    }

    // On prend les infos de l'utilisateur
    $user = $this->authComponent->getUser();

    // Message de réussite d'identification
    $this->request->getSession()->setFlash('success', 'Vous êtes maintenant identifié.');

    // On redirige l'utilisateur
    return new RedirectResponse('/dashboard');
  }

  public function logout(): Response
  {
    // Déconnexion de l'utilisateur
    $this->authComponent->logout();

    // On définit un message de déconnexion
    $this->request->getSession()->setFlash('success', 'Bye...');

    // On redirige l'utilisateur
    return new RedirectResponse('/login');
  }
}