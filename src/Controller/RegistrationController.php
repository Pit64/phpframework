<?php

namespace App\Controller;

use App\Form\User\RegistrationForm;
use App\Repository\UserMapper;
use Pit64\Framework\Authentication\SessionAuthentication;
use Pit64\Framework\Controller\AbstractController;
use Pit64\Framework\Http\RedirectResponse;
use Pit64\Framework\Http\Response;

class RegistrationController extends AbstractController
{

  public function __construct(private UserMapper $userMapper, private SessionAuthentication $authComponent)
  {
  }

  public function index(): Response
  {
    return $this->render('register.html.twig');
  }

  public function register(): Response
  {
    // Création d'un modèle de formulaire qui pourra :
    // - valider les champs
    // - mapper les champs dans les propriétés de l'objet User
    // - à la fin enregistrer l'utilisateur dans la base de données
    $form = new RegistrationForm($this->userMapper);
    $form->setFields($this->request->input('username'), $this->request->input('password'));

    // Validation
    if ($form->hasValidationErrors())
    {
      // On ajoute l'erreur dans la session et on redirige l'utilisateur
      foreach ($form->getValidationErrors() AS $error)
      {
        $this->request->getSession()->setFlash('error', $error);
      }

      return new RedirectResponse('/register');
    }

    // On enregistre l'utilisateur avec $form->save()
    $user = $form->save();

    // On ajoute un message de réussite d'identification
    $this->request->getSession()->setFlash('success', sprintf('Utilisateur %s crée avec succès.', $user->getUsername()));

    // On identifie l'utilisateur
    $this->authComponent->login($user);

    // On redirige l'utilisateur
    return new RedirectResponse('/');
  }
}