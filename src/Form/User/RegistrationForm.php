<?php

namespace App\Form\User;

use App\Entity\User;
use App\Repository\UserMapper;

class RegistrationForm
{
  private string $username;
  private string $password;
  private array $errors = [];

  public function __construct(private UserMapper $userMapper)
  {
  }

  public function setFields(string $username, string $password): void
  {
    $this->username = $username;
    $this->password = $password;
  }

  public function save(): User
  {
    $user = User::create($this->username, $this->password);

    $this->userMapper->save($user);

    return $user;
  }

  public function hasValidationErrors(): bool
  {
    return count($this->getValidationErrors()) > 0;
  }

  public function getValidationErrors(): array
  {
    if (!empty($this->errors))
    {
      return $this->errors;
    }

    // Longueur de l'identifiant
    if (strlen($this->username) < 5 || strlen($this->username) > 20)
    {
      $this->errors[] = 'L\'identifiant doit être compris entre 5 et 20 caractères.';
    }

    // Type de caractères de l'identifiant
    if (!preg_match('/^\w+$/', $this->username))
    {
      $this->errors[] = 'L\'identifiant doit contenir des mots dans espace.';
    }

    // Longueur du mot de passe
    if (strlen($this->password) < 8)
    {
      $this->errors[] = 'Le mot de passe doit contenir au minimum 8 caractères.';
    }

    return $this->errors;
  }
}