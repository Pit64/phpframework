<?php

namespace App\Provider;

use App\EventListener\ContentLengthListener;
use App\EventListener\InternalErrorListener;
use Pit64\Framework\Dbal\Event\PostPersist;
use Pit64\Framework\EventDispatcher\EventDispatcher;
use Pit64\Framework\Http\Event\ResponseEvent;
use Pit64\Framework\ServiceProvider\ServiceProviderInterface;

class EventServiceProvider implements ServiceProviderInterface
{
  private array $listen = [
    ResponseEvent::class => [
      InternalErrorListener::class,
      ContentLengthListener::class
    ],
    PostPersist::class => [
    ]
  ];

  public function __construct(private EventDispatcher $eventDispatcher)
  {
  }

  public function register(): void
  {
    foreach ($this->listen AS $eventName => $listeners)
    {
      foreach (array_unique($listeners) AS $listener)
      {
        $this->eventDispatcher->addListener($eventName, new $listener());
      }
    }
  }
}