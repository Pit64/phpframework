<?php

$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->load(BASE_PATH . '/.env');

$container = new \League\Container\Container();

$container->delegate(new \League\Container\ReflectionContainer(true));

// Paramètres
$basePath = dirname(__DIR__);
$container->add('basePath', new \League\Container\Argument\Literal\StringArgument($basePath));
$routes = require_once($basePath . '/routes/web.php');
$appEnv = $_SERVER['APP_ENV'];
$templatesPath = $basePath . '/templates';

$container->add('APP_ENV', new \League\Container\Argument\Literal\StringArgument($appEnv));
$databaseUrl = 'sqlite:///' . $basePath . '/var/db.sqlite';

$container->add(
  'base-commands-namespace',
  new \League\Container\Argument\Literal\StringArgument('Pit64\\Framework\\Console\\Command\\')
);

// Services
$container->add(
  Pit64\Framework\Routing\RouterInterface::class,
  Pit64\Framework\Routing\Router::class
);

$container->extend(Pit64\Framework\Routing\RouterInterface::class)
          ->addMethodCall(
            'setRoutes',
            [new \League\Container\Argument\Literal\ArrayArgument($routes)]
          );

$container->add(
  Pit64\Framework\Http\Middleware\RequestHandlerInterface::class,
  Pit64\Framework\Http\Middleware\RequestHandler::class)
          ->addArgument($container);

$container->addShared(Pit64\Framework\EventDispatcher\EventDispatcher::class);

$container->add(Pit64\Framework\Http\Kernel::class)
          ->addArguments([
            $container,
            Pit64\Framework\Http\Middleware\RequestHandlerInterface::class,
            Pit64\Framework\EventDispatcher\EventDispatcher::class
          ]);

$container->add(Pit64\Framework\Console\Application::class)
          ->addArgument($container);

$container->add(Pit64\Framework\Console\Kernel::class)
          ->addArguments([$container, Pit64\Framework\Console\Application::class]);

$container->addShared(
  Pit64\Framework\Session\SessionInterface::class,
  Pit64\Framework\Session\Session::class
);

$container->add('template-renderer-factory', Pit64\Framework\Template\TwigFactory::class)
          ->addArguments([
            Pit64\Framework\Session\SessionInterface::class,
            new \League\Container\Argument\Literal\StringArgument($templatesPath)
          ]);

$container->addShared('twig', function () use ($container) {
  return $container->get('template-renderer-factory')->create();
});

$container->add(Pit64\Framework\Controller\AbstractController::class);

$container->inflector(Pit64\Framework\Controller\AbstractController::class)
          ->invokeMethod('setContainer', [$container]);

$container->add(Pit64\Framework\Dbal\ConnectionFactory::class)
          ->addArguments([
            new \League\Container\Argument\Literal\StringArgument($databaseUrl)
          ]);

$container->addShared(\Doctrine\DBAL\Connection::class, function() use ($container): \Doctrine\DBAL\Connection {
  return $container->get(Pit64\Framework\Dbal\ConnectionFactory::class)->create();
});

$container->add('database:migrations:migrate', Pit64\Framework\Console\Command\MigrateDatabase::class)
          ->addArguments([
            \Doctrine\DBAL\Connection::class,
            new \League\Container\Argument\Literal\StringArgument($basePath . '/migrations')
          ]);

$container->add(Pit64\Framework\Http\Middleware\RouterDispatch::class)
          ->addArguments([
            Pit64\Framework\Routing\RouterInterface::class,
            $container
          ]);

$container->add(Pit64\Framework\Authentication\SessionAuthentication::class)
          ->addArguments([
            App\Repository\UserRepository::class,
            Pit64\Framework\Session\SessionInterface::class
          ]);

$container->add(Pit64\Framework\Http\Middleware\ExtractRouteInfo::class)
          ->addArgument(new \League\Container\Argument\Literal\ArrayArgument($routes));

return $container;