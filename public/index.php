<?php declare(strict_types=1);

use Pit64\Framework\Http\Kernel;
use Pit64\Framework\Http\Request;

define('BASE_PATH', dirname(__DIR__));

require_once(BASE_PATH . '/vendor/autoload.php');

$container = require_once(BASE_PATH . '/config/services.php');

// Bootstrapping
require_once(BASE_PATH . '/bootstrap/bootstrap.php');

// Réception de la requête
$request = Request::createFromGlobals();

// Exécution de quelques logiques
$kernel = $container->get(Kernel::class);

// Envoi de la réponse
$response = $kernel->handle($request);

$response->send();

$kernel->terminate($request, $response);