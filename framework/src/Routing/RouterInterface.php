<?php

namespace Pit64\Framework\Routing;

use Pit64\Framework\Http\Request;
use Psr\Container\ContainerInterface;

interface RouterInterface
{
  public function dispatch(Request $request, ContainerInterface $container);

  public function setRoutes(array $routes): void;
}