<?php

namespace Pit64\Framework\EventDispatcher;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\StoppableEventInterface;

class EventDispatcher implements EventDispatcherInterface
{
  private iterable $listeners = [];

  public function dispatch(object $event): object
  {
    // On boucle sur les listeners pour l'événement
    foreach ($this->getListenersForEvent($event) AS $listener)
    {
      // On arrête tout si la propagation est stoppée
      if ($event instanceof StoppableEventInterface && $event->isPropagationStopped())
      {
        return $event;
      }

      // On appelle le listener, on lui passe l'événement (chaque listener doit pouvoir être appelé)
      $listener($event);
    }

    return $event;
  }

  public function addListener(string $eventName, callable $listener): self
  {
    $this->listeners[$eventName][] = $listener;

    return $this;
  }

  public function getListenersForEvent(object $event): iterable
  {
    $eventName = get_class($event);

    if (array_key_exists($eventName, $this->listeners))
    {
      return $this->listeners[$eventName];
    }

    return [];
  }
}