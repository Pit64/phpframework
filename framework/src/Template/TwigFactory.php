<?php

namespace Pit64\Framework\Template;

use Pit64\Framework\Session\SessionInterface;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class TwigFactory
{
  public function __construct(private SessionInterface $session, private string $templatesPath)
  {
  }

  public function create(): Environment
  {
    // On instancie FileSystemLoader avec le chemin des modèles
    $loader = new FilesystemLoader($this->templatesPath);

    // On instancie l'environnement de Twig avec le chargeur
    $twig = new Environment($loader, [
      'debug' => true,
      'cache' => false
    ]);

    // Ajout de la fonction de session de Twig à l'environnement
    $twig->addExtension(new DebugExtension());
    $twig->addFunction(new TwigFunction('session', [$this, 'getSession']));

    return $twig;
  }

  public function getSession(): SessionInterface
  {
    return $this->session;
  }
}