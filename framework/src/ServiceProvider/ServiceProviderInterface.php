<?php

namespace Pit64\Framework\ServiceProvider;

interface ServiceProviderInterface
{
  public function register(): void;
}