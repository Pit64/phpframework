<?php

namespace Pit64\Framework\Container;

use Pit64\Framework\Tests\DependantClass;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class Container implements ContainerInterface
{
  private array $services = [];

  public function add(string $id, string|object $concrete = null)
  {
    if (null === $concrete)
    {
      if (!class_exists($id))
      {
        throw new ContainerException("Service $id could not be found");
      }

      $concrete = $id;
    }

    $this->services[$id] = $concrete;
  }

  public function get(string $id)
  {
    if (!$this->has($id))
    {
      if (!class_exists($id))
      {
        throw new ContainerException("Service $id could not be resolved");
      }

      $this->add($id);
    }

    $object = $this->resolve($this->services[$id]);

    return $object;
  }

  private function resolve($class): object
  {
    // On instancie la classe Reflexion (dump et check)
    $reflectionClass = new \ReflectionClass($class);

    // On utilise Reflexion pour tenter d'obtenir le constructeur de la classe
    $constructor = $reflectionClass->getConstructor();

    // Si il n'y a pas de constructeur, on ne fait qu'une simple instanciation
    if (null === $constructor)
    {
      return $reflectionClass->newInstance();
    }

    // On récupère les paramètres du constructeur
    $constructorParams = $constructor->getParameters();

    // On obtient ses dépendances
    $classDependencies = $this->resolveClassDependencies($constructorParams);

    // On instancie avec les dépendances
    $service = $reflectionClass->newInstanceArgs($classDependencies);

    // On retourne l'objet
    return $service;
  }

  private function resolveClassDependencies(array $reflectionParameters): array
  {
    // On initialise un tableau vide concernant les dépendances (requis par newInstanceArgs)
    $classDependencies = [];

    // On tente de localiser et d'instancier chaque paramètre
    foreach ($reflectionParameters AS $parameter)
    {
      // On récupère les paramètres de ReflectionNamedType dans $serviceType
      $serviceType = $parameter->getType();

      // On tente d'instancier avec le nom de $serviceType
      $service = $this->get($serviceType->getName());

      // On ajoute le service dans le tableau $classDependencies
      $classDependencies[] = $service;
    }

    // On retourne la tableau de dépendances
    return $classDependencies;
  }

  public function has(string $id): bool
  {
    return array_key_exists($id, $this->services);
  }
}