<?php

namespace Pit64\Framework\Console\Command;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;

class MigrateDatabase implements CommandInterface
{
  private string $name = 'database:migrations:migrate';

  public function __construct(private Connection $connection, private string $migrationsPath)
  {
  }

  public function execute(array $params = []): int
  {
    try
    {
      // Création de la table de migrations si elle n'existe pas
      $this->createMigrationsTable();

      $this->connection->beginTransaction();

      // On appelle $appliedMigrations qui se trouve déjà dans la table database.migrations
      $appliedMigrations = $this->getAppliedMigrations();

      // Obtention de $migrationFiles depuis le dossier des migrations
      $migrationFiles = $this->getMigrationFiles();

      // Obtention des migrations à appliquer (ce qui se trouve dans $migrationFiles mais pas dans $appliedMigrations)
      $migrationsToApply = array_diff($migrationFiles, $appliedMigrations);

      $schema = new Schema();

      // On crée les requêtes SQL pour toutes les migrations qui n'ont pas été exécutées (qui ne sont pas dans la base de données)
      foreach ($migrationsToApply AS $migration)
      {
        // On importe l'objet
        $migrationObject = require_once($this->migrationsPath . '/' . $migration);

        // On appelle la méthode
        $migrationObject->up($schema);

        // On ajoute la migration dans la base de données
        $this->insertMigration($migration);
      }

      // Exécution des requêtes SQL
      $sqlArray = $schema->toSql($this->connection->getDatabasePlatform());

      foreach ($sqlArray AS $sql)
      {
        $this->connection->executeQuery($sql);
      }

      $this->connection->commit();

      return 0;
    }
    catch (\Throwable $throwable)
    {
      $this->connection->rollBack();

      throw $throwable;
    }
  }

  private function insertMigration(string $migration): void
  {
    $sql = "INSERT INTO migrations (migration) VALUES (?)";

    $statement = $this->connection->prepare($sql);
    $statement->bindValue(1, $migration);
    $statement->executeStatement();
  }

  private function getMigrationFiles(): array
  {
    $migrationFiles = scandir($this->migrationsPath);

    $filteredFiles = array_filter($migrationFiles, function($file) {
      return !in_array($file, ['.', '..']);
    });

    return $filteredFiles;
  }

  private function getAppliedMigrations(): array
  {
    $sql = "SELECT migration FROM migrations";

    $appliedMigrations = $this->connection->executeQuery($sql)->fetchFirstColumn();

    return $appliedMigrations;
  }

  private function createMigrationsTable(): void
  {
    $schemaManager = $this->connection->createSchemaManager();

    if (!$schemaManager->tablesExist('migrations'))
    {
      $schema = new Schema();
      $table = $schema->createTable('migrations');
      $table->addColumn('id', Types::INTEGER, ['unsigned' => true, 'autoincrement' => true]);
      $table->addColumn('migration', Types::STRING);
      $table->addColumn('created_at', Types::DATETIME_IMMUTABLE, ['default' => 'CURRENT_TIMESTAMP']);
      $table->setPrimaryKey(['id']);

      $sqlArray = $schema->toSql($this->connection->getDatabasePlatform());

      $this->connection->executeQuery($sqlArray[0]);

      echo 'Table de migration crée' . PHP_EOL;
    }
  }
}