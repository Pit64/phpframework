<?php

namespace Pit64\Framework\Console\Command;

interface CommandInterface
{
  public function execute(array $params = []): int;
}