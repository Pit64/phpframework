<?php

namespace Pit64\Framework\Console;

use Pit64\Framework\Console\Command\CommandInterface;
use Psr\Container\ContainerInterface;

final class Kernel
{
  public function __construct(private ContainerInterface $container, private Application $application)
  {
  }

  public function handle(): int
  {
    // Inscription des commandes dans le conteneur
    $this->registerCommands();

    // Exécution de l'application dans la console, retournant un code statut
    $status = $this->application->run();

    // On retourne le code statut
    return $status;
  }

  private function registerCommands(): void
  {
    // Inscription de toutes les commandes internes
    // On va chercher tous les fichiers dans le dossier Command
    $commandFiles = new \DirectoryIterator(__DIR__ . '/Command');

    $namespace = $this->container->get('base-commands-namespace');

    // On boucle sur chacun des fichiers trouvés
    foreach ($commandFiles AS $commandFile)
    {
      // Vérification que chaque élément trouvé est un fichier
      if (!$commandFile->isFile())
      {
        continue;
      }

      // On va chercher le nom de la classe de la commande en utilisant psr4, ceci sera le même que le nom du fichier
      $command = $namespace.pathinfo($commandFile, PATHINFO_FILENAME);

      // On vérifie si c'est bien une sous-classe de CommandInterface
      if (is_subclass_of($command, CommandInterface::class))
      {
        // Ajout dan le conteneur, utilisant le nom comme ID. Par exemple $container->add('database:migrations:migrate', MigrateDatabase::class);
        $commandName = (new \ReflectionClass($command))->getProperty('name')->getDefaultValue();

        $this->container->add($commandName, $command);
      }
    }
  }
}