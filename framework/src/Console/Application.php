<?php

namespace Pit64\Framework\Console;

use Psr\Container\ContainerInterface;

class Application
{
  public function __construct(private ContainerInterface $container)
  {
  }

  public function run(): int
  {
    // On utilise les variables d'environnements pour obtenir le nom de la commande
    $argv = $_SERVER['argv'];
    $commandName = $argv[1] ?? null;

    // Envoie une exception si aucun nom de commande n'est fourni
    if (!$commandName || FALSE === $this->container->has($commandName))
    {
      throw new ConsoleException('Un nom de commande doit être fourni');
    }

    // On utilise le nom de la commande pour obtenir un objet commande depuis le conteneur
    $command = $this->container->get($commandName);

    // On parse les variables pour obtenir les options et arguments
    $args = array_slice($argv, 2);
    $options = $this->parseOptions($args);

    // Exécution de la commande, retournant le code statut
    $status = $command->execute($options);

    // Retour du code statut
    return $status;
  }

  private function parseOptions(array $args): array
  {
    $options = [];

    foreach ($args AS $arg)
    {
      if (str_starts_with($arg, '--'))
      {
        // C'est bien une option
        $option = explode('=', substr($arg, 2));
        $options[$option[0]] = $option[1] ?? true;
      }
    }

    return $options;
  }
}