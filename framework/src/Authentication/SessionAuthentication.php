<?php

namespace Pit64\Framework\Authentication;

use Pit64\Framework\Session\Session;
use Pit64\Framework\Session\SessionInterface;

class SessionAuthentication implements SessionAuthInterface
{
  private AuthUserInterface $user;

  public function __construct(private AuthRepositoryInterface $authRepository, private SessionInterface $session)
  {
  }

  public function authenticate(string $username, string $password): bool
  {
    // On va chercher l'utilisateur avec son identifiant
    $user = $this->authRepository->findByUsername($username);

    // On vérifie si l'utilisateur existe
    if (!$user)
    {
      // L'utilisateur n'existe pas
      return false;
    }

    // Est-ce que le mot de passe correspond ?
    if (!password_verify($password, $user->getPassword())) {
      // Si non, on retourne faux
      return false;
    }
    // On identifie l'utilisateur
    $this->login($user);

    // On retourne vrai
    return true;
  }

  public function login(AuthUserInterface $user)
  {
    // On ouvre la session
    $this->session->start();

    // Ajout de l'id de l'utilisateur dans la session
    $this->session->set(Session::AUTH_KEY, $user->getAuthId());

    // Définition de l'utilisateur identifié
    $this->user = $user;
  }

  public function logout()
  {
    // On supprime la session
    $this->session->remove(Session::AUTH_KEY);
  }

  public function getUser(): AuthUserInterface
  {
    // On obtient les informations de l'utilisateur
    return $this->user;
  }
}