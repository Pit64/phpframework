<?php

namespace Pit64\Framework\Authentication;

interface AuthRepositoryInterface
{
  public function findByUsername(string $username): ?AuthUserInterface;
}