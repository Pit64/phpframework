<?php

namespace Pit64\Framework\Dbal\Event;

use Pit64\Framework\Dbal\Entity;
use Pit64\Framework\EventDispatcher\Event;

class PostPersist extends Event
{
  public function __construct(private Entity $subject)
  {
  }
}