<?php

namespace Pit64\Framework\Http\Middleware;

use Pit64\Framework\Http\Request;
use Pit64\Framework\Http\Response;

interface MiddlewareInterface
{
  public function process(Request $request, RequestHandlerInterface $requestHandler): Response;
}