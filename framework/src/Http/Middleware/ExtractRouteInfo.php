<?php

namespace Pit64\Framework\Http\Middleware;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Pit64\Framework\Http\HttpException;
use Pit64\Framework\Http\HttpRequestMethodException;
use Pit64\Framework\Http\Request;
use Pit64\Framework\Http\Response;
use function FastRoute\simpleDispatcher;

class ExtractRouteInfo implements MiddlewareInterface
{
  public function __construct(private array $routes)
  {
  }

  public function process(Request $request, RequestHandlerInterface $requestHandler): Response
  {
    // Création d'un dispatcher
    $dispatcher = simpleDispatcher(function (RouteCollector $routeCollector) {
      foreach ($this->routes AS $route)
      {
        $routeCollector->addRoute(...$route);
      }
    });

    // Dispatch d'une URI pour obtenir les informations de la route
    $routeInfo = $dispatcher->dispatch(
      $request->getMethod(),
      $request->getPathInfo()
    );

    switch ($routeInfo[0])
    {
      case Dispatcher::FOUND:
        // On définit $request->routeHandler
        $request->setRouteHandler($routeInfo[1]);

        // On définit $request->routeHandlerArgs
        $request->setRouteHandlerArgs($routeInfo[2]);

        // Injection du middleware de la route dans le gestionnaire (handler)
        if (is_array($routeInfo[1]) && isset($routeInfo[1][2]))
        {
          $requestHandler->injectMiddleware($routeInfo[1][2]);
        }
        break;
      case Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = implode(', ', $routeInfo[1]);
        $e = new HttpRequestMethodException("Les méthodes autorisées sont $allowedMethods");
        $e->setStatusCode(405);
        throw $e;
      default:
        $e = new HttpException('Non trouvé');
        $e->setStatusCode(404);
        throw $e;
    }

    return $requestHandler->handle($request);
  }
}