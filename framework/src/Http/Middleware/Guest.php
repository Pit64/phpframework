<?php

namespace Pit64\Framework\Http\Middleware;

use Pit64\Framework\Authentication\SessionAuthentication;
use Pit64\Framework\Http\RedirectResponse;
use Pit64\Framework\Http\Request;
use Pit64\Framework\Http\Response;
use Pit64\Framework\Session\Session;
use Pit64\Framework\Session\SessionInterface;

class Guest implements MiddlewareInterface
{
  public function __construct(private SessionInterface $session)
  {
  }

  public function process(Request $request, RequestHandlerInterface $requestHandler): Response
  {
    $this->session->start();

    if ($this->session->isAuthenticated())
    {
      return new RedirectResponse('/dashboard');
    }

    return $requestHandler->handle($request);
  }
}