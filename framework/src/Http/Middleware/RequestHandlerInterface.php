<?php

namespace Pit64\Framework\Http\Middleware;

use Pit64\Framework\Http\Request;
use Pit64\Framework\Http\Response;

interface RequestHandlerInterface
{
  public function handle(Request $request): Response;
}