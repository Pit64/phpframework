<?php

namespace Pit64\Framework\Http\Middleware;

use Pit64\Framework\Http\Request;
use Pit64\Framework\Http\Response;
use Pit64\Framework\Http\TokenMismatchException;

class VerifyCsrfToken implements MiddlewareInterface
{

  public function process(Request $request, RequestHandlerInterface $requestHandler): Response
  {
    // On procède si aucun changement n'est survenue dans la requête
    if (!in_array($request->getMethod(), ['POST', 'PUT', 'PATCH', 'DELETE']))
    {
      return $requestHandler->handle($request);
    }

    // On rapatrie les jetons
    $tokenFromSession = $request->getSession()->get('csrf_token');
    $tokenFromRequest = $request->input('_token');

    // On vérifie si les 2 jetons sont les mêmes
    if (!hash_equals($tokenFromSession, $tokenFromRequest))
    {
      // Les jetons ne sont pas les même, on renvoie une erreur
      $exception = new TokenMismatchException('Votre requête ne peut pas être validée. Veuillez recommencer.');
      $exception->setStatusCode(Response::HTTP_FORBIDDEN);
      throw $exception;
    }

    return $requestHandler->handle($request);
  }
}