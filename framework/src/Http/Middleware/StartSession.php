<?php

namespace Pit64\Framework\Http\Middleware;

use Pit64\Framework\Http\Request;
use Pit64\Framework\Http\Response;
use Pit64\Framework\Session\SessionInterface;

class StartSession implements MiddlewareInterface
{
  public function __construct(private SessionInterface $session, private string $apiPrefix = '/api/')
  {
  }

  public function process(Request $request, RequestHandlerInterface $requestHandler): Response
  {
    if (!str_starts_with($request->getPathInfo(), $this->apiPrefix)) {
      $this->session->start();

      $request->setSession($this->session);
    }

    return $requestHandler->handle($request);
  }
}