<?php

namespace Pit64\Framework\Http\Middleware;

use Pit64\Framework\Container\Container;
use Pit64\Framework\Http\Request;
use Pit64\Framework\Http\Response;
use Psr\Container\ContainerInterface;

class RequestHandler implements RequestHandlerInterface
{
  private array $middleware = [
    ExtractRouteInfo::class,
    StartSession::class,
    VerifyCsrfToken::class,
    RouterDispatch::class
  ];

  public function __construct(private ContainerInterface $container)
  {
  }

  public function handle(Request $request): Response
  {
    // Si il n'y a pas de classe middleware à exécuter, une réponse par défaut doit être retournée
    // Une réponse doit avoir été retournée avant qu'elle ne soit vide.
    if (empty($this->middleware))
    {
      return new Response('C\'est tout pété. Contacte l\'assistance.', 500);
    }

    // On prend le prochain middleware à exécuter
    $middlewareClass = array_shift($this->middleware);

    $middleware = $this->container->get($middlewareClass);

    // On crée une nouvelle instance du middleware et on appelle process
    $response = $middleware->process($request, $this);

    return $response;
  }

  public function injectMiddleware(array $middleware): void
  {
    array_splice($this->middleware, 0, 0, $middleware);
  }
}