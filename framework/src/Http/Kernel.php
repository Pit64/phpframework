<?php

namespace Pit64\Framework\Http;

use Doctrine\DBAL\Connection;
use Pit64\Framework\EventDispatcher\EventDispatcher;
use Pit64\Framework\Http\Event\ResponseEvent;
use Pit64\Framework\Http\Middleware\RequestHandlerInterface;
use Pit64\Framework\Routing\Router;
use Pit64\Framework\Routing\RouterInterface;
use Psr\Container\ContainerInterface;

class Kernel
{
  private string $appEnv;

  public function __construct(private ContainerInterface $container, private RequestHandlerInterface $requestHandler, private EventDispatcher $eventDispatcher)
  {
    $this->appEnv = $this->container->get('APP_ENV');
	}

  public function handle(Request $request): Response
  {
    try {
      $response = $this->requestHandler->handle($request);

    } catch (\Exception $exception) {
      $response = $this->createExceptionResponse($exception);
    }

    $this->eventDispatcher->dispatch(new ResponseEvent($request, $response));

    return $response;
	}

  /**
   * @throws \Exception $exception
   */
  private function createExceptionResponse(\Exception $exception): Response
  {
    if (in_array($this->appEnv, ['dev', 'test']))
    {
      throw $exception;
    }

    if ($exception instanceof HttpException)
    {
      return new Response($exception->getMessage(), $exception->getStatusCode());
    }

    return new Response('Internal Server Error', Response::HTTP_INTERNAL_SERVER_ERROR);
  }

  public function terminate(Request $request, Response $response): void
  {
    $request->getSession()?->clearFlash();
  }
}