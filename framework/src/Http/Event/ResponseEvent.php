<?php

namespace Pit64\Framework\Http\Event;

use Pit64\Framework\EventDispatcher\Event;
use Pit64\Framework\Http\Request;
use Pit64\Framework\Http\Response;

class ResponseEvent extends Event
{
  public function __construct(private Request $request, private Response $response)
  {
  }

  public function getRequest(): Request
  {
    return $this->request;
  }

  public function getResponse(): Response
  {
    return $this->response;
  }


}