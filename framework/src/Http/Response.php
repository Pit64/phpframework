<?php

namespace Pit64\Framework\Http;

class Response
{
  public const int HTTP_INTERNAL_SERVER_ERROR = 500;
  public const int HTTP_FORBIDDEN = 403;

	public function __construct(
		private ?string $content = '',
		private int $status = 200,
		private array $headers = []
	)
	{
    // Ceci doit être défini avant d'envoyer le contenu
    // Il est préférable de créer l'instantiation ici
    http_response_code($this->status);
	}

	public function send(): void
	{
    // Début du buffer en sortie
    ob_start();

    // Envoi des en-têtes
    foreach ($this->headers AS $key => $value)
    {
      header("$key: $value");
    }

    // On ajoute la contenu au buffer
		echo $this->content;

    // Vidage du buffer avec envoi vers le client
    ob_end_flush();
	}

  public function setContent(?string $content): void
  {
    $this->content = $content;
  }

  public function getStatus(): int
  {
    return $this->status;
  }

  public function setStatus(int $status): void
  {
    $this->status = $status;
  }

  public function getHeader(string $header): mixed
  {
    return $this->headers[$header];
  }

  public function getHeaders(): array
  {
    return $this->headers;
  }

  public function setHeader($key, $value): void
  {
    $this->headers[$key] = $value;
  }

  public function getContent(): ?string
  {
    return $this->content;
  }
}