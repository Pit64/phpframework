<?php

namespace Pit64\Framework\Tests;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Pit64\Framework\Session\Session;

class SessionTest extends TestCase
{
  protected function setUp(): void
  {
    unset($_SESSION);
  }

  #[Test]
  public function testSetAndGetFlash(): void
  {
    $session = new Session();
    $session->setFlash('success', 'Beau travail !');
    $session->setFlash('error', 'Mauvais travail !');
    $this->assertTrue($session->hasFlash('success'));
    $this->assertTrue($session->hasFlash('error'));
    $this->assertEquals(['Beau travail !'], $session->getFlash('success'));
    $this->assertEquals(['Mauvais travail !'], $session->getFlash('error'));
    $this->assertEquals([], $session->getFlash('warning'));
  }
}