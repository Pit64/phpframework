<?php

namespace Pit64\Framework\Tests;

use Pit64\Framework\Container\Container;
use Pit64\Framework\Container\ContainerException;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
  #[Test]
  public function a_service_can_be_retrieved_from_the_container()
  {
    // Setup
    $container = new Container();

    // On effectue une manœuvre
    $container->add('dependant-class', DependantClass::class);

    // On effectue le test
    $this->assertInstanceOf(DependantClass::class, $container->get('dependant-class'));
  }

  #[Test]
  public function a_ContainerException_is_thrown_if_a_service_cannot_be_found()
  {
    // Setup
    $container = new Container();

    // On effectue une manœuvre
    $this->expectException(ContainerException::class);

    // On effectue le test
    $container->add('foobar');
  }

  #[Test]
  public function can_check_if_the_container_has_a_service(): void
  {
    // Setup
    $container = new Container();

    // On effectue une manœuvre
    $container->add('dependant-class', DependantClass::class);

    // On effectue les tests
    $this->assertTrue($container->has('dependant-class'));
    $this->assertFalse($container->has('non-existent-class'));
  }

  #[Test]
  public function services_can_be_recursively_autowired()
  {
    // Setup
    $container = new Container();

    // On effectue une manœuvre
    $dependantService = $container->get(DependantClass::class);
    $dependancyService = $dependantService->getDependency();

    // On effectue les tests
    $this->assertInstanceOf(DependencyClass::class, $dependancyService);
    $this->assertInstanceOf(SubDependencyClass::class, $dependancyService->getSubDependency());
  }
}