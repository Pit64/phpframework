<?php

$providers = [
  \App\Provider\EventServiceProvider::class
];

foreach ($providers AS $providerClass)
{
  $provider = $container->get($providerClass);
  $provider->register();
}
